#! /usr/bin/env python3

from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import sys, os, subprocess

class Widget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent, Qt.Popup)
        desktopRect = getTotalScreenGeometry()
        self.aspect = getAspectRatio()
        self.resize(desktopRect.size())
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.tabletRect = getPreviousRect()
        setTabletArea(desktopRect, save=False)
        self.moveMode = False
        self.setMouseTracking(True)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setBrush(QColor(80, 100, 255, 50))
        painter.setPen(QPen(QColor(0, 150, 255)))
        painter.drawRect(getScreenGeometry())

        painter.setBrush(QColor(255, 150, 0, 50))
        painter.setPen(QPen(QColor(255, 150, 0)))
        painter.drawRect(self.tabletRect)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Shift:
            self.moveMode = True
            self.start = QCursor.pos()
            self.offset = self.start - self.tabletRect.topLeft()

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Space:
            desktopRect = getTotalScreenGeometry()
            setTabletArea(desktopRect, save=False)
            sys.exit()
        elif event.key() == Qt.Key_Shift:
            self.update()
            setTabletArea(self.tabletRect, save=True)
            self.tabletRect = QRect()
            sys.exit()
        elif event.key() == Qt.Key_Escape:
            sys.exit()

    def mousePressEvent(self, event):
        self.start = event.screenPos().toPoint()
        self.offset = self.start - self.tabletRect.topLeft()

        if not self.moveMode:
            self.tabletRect.setTopLeft(self.start)
            self.tabletRect.setBottomRight(self.start)
            self.update()

    def mouseMoveEvent(self, event):
        if self.moveMode:
            self.start = event.screenPos()
            self.tabletRect.moveTopLeft(self.start.toPoint() - self.offset)
        elif event.buttons() == Qt.LeftButton:
            self.tabletRect.setBottomRight(event.screenPos().toPoint())

            s = self.tabletRect.size()
            aspect = s.height() / s.width()

            size = self.tabletRect.size().height() * self.aspect / aspect
            self.tabletRect.setHeight(size)

        desktopRect = getTotalScreenGeometry()

        if self.tabletRect.left() < 0:
            self.tabletRect.moveLeft(0)
        if self.tabletRect.right() > desktopRect.width():
            self.tabletRect.moveRight(desktopRect.width())
        if self.tabletRect.top() < 0:
            self.tabletRect.moveTop(0)
        if self.tabletRect.bottom() > desktopRect.height():
            self.tabletRect.moveBottom(desktopRect.height())

        self.update()

    def mouseReleaseEvent(self, event):
        self.update()
        if self.tabletRect.width() * self.tabletRect.height() < 100:
            self.tabletRect = getScreenGeometry()
        setTabletArea(self.tabletRect, save=True)
        self.tabletRect = QRect()
        sys.exit()

def getTotalScreenGeometry():
    rect = QGuiApplication.screens()[0].geometry()
    for screen in QGuiApplication.screens():
        rect |= screen.geometry()

    return rect

def getScreenGeometry():
    pos = QCursor.pos()
    screen = QGuiApplication.screenAt(QCursor.pos())
    return screen.geometry()

def getPreviousRect():
    config_path = os.getenv("HOME") + "/.config/screen_mapper"
    if not os.path.exists(config_path):
        os.mkdir(config_path)
        return QRect()

    if not os.path.exists(config_path + "/mapping"):
        return QRect()

    with open(config_path + "/mapping", "r") as f:
        w, h, x, y = [int(x) for x in f.readline().replace("\n", "").split(",")]

    return QRect(QPoint(x, y), QSize(w, h))

def getTablet(part="stylus"):
    output = str(subprocess.check_output(["sh", "-c", "xinput --list | grep -i wacom"]))
    output = output.replace("\\xe2\\x8e\\x9c", "").replace("\\t","")
    output = output.replace("\\xe2\\x86\\xb3", "")

    output = [o[o.find("Wacom"):o.find("id=")].strip() for o in output.split("\\n")]
    output = [o for o in output if o != ""]

    for o in output:
        if o.find(part) > 0:
            return o

def getAspectRatio():
    tab = getTablet()
    output = str(subprocess.check_output(["sh", "-c", "xsetwacom --get \"{}\" Area".format(tab)])).replace("\\n","").strip()
    output = output[output.find("'")+1:output.rfind("'")]
    _, _, w, h = [int(o) for o in output.split()]
    return h / w

def setTabletArea(rect, save=False):
    w, h, x, y = (rect.width(),
                    rect.height(),
                    rect.topLeft().x(),
                    rect.topLeft().y())

    rect = "{}x{}+{}+{}".format(w, h, x, y)

    print("Map output to: " + rect)
    tab = getTablet()
    os.system("xsetwacom --set \"{}\" MapToOutput {}".format(tab, rect))

    if save:
        config_path = os.getenv("HOME") + "/.config/screen_mapper"
        if not os.path.exists(config_path):
            os.mkdir(config_path)

        with open(config_path + "/mapping", "w") as f:
            f.write("{},{},{},{}\n".format(w, h, x, y))

def main():
    app = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    app.exec_()

if __name__ == "__main__":
    main()
